module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'airbnb-typescript',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
  ],
  ignorePatterns: ['**/node_modules/**/*'],
};

// "no-use-before-define": ["error", { "variables": false, "classes": true }]
