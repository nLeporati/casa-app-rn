import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';
import * as storage from '../utils/storage';

interface AxiosConfig {
  handlerEnabled: boolean;
}

const axiosClient: AxiosInstance = axios.create({
  baseURL: 'https://casa-app-nestjs.herokuapp.com/',
  // baseURL: 'http://ec2-3-92-201-49.compute-1.amazonaws.com:3000/',
  // baseURL: 'http://192.168.1.83:3000/',
});

const isHandlerEnabled = (config: any = {}) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled
    ? false
    : true;
};

const requestHandler = async (request: AxiosRequestConfig) => {
  // if (isHandlerEnabled(request)) {
  //   // Modify request here
  // }
  const token = await storage.loadString('jwtToken');
  if (token) {
    request.headers['Authorization'] = `Bearer ${token}`;
  }
  console.log('axios url', request.url);
  return request;
};

axiosClient.interceptors.request.use(request => requestHandler(request));

export default axiosClient;
