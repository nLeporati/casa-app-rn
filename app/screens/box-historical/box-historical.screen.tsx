import React, {useEffect, useState} from 'react';
import {BoxModel} from '../../models/box.model';
import {View, StyleSheet, ScrollView} from 'react-native';
import Currency from '../../components/currency';
import {Title} from 'react-native-paper';
import {NavigationStackProp} from 'react-navigation-stack';
import DATA_BOXES from '../../data/data-boxes';
import {spacing, color} from '../../theme';
import Charge from '../../components/charge';
import {ChargeModel} from '../../models/charge.model';
import {DATA_COLLECTIONS} from '../../data/data-collections';
import CollectionList from '../../components/collection-list';
import BoxService from '../../services/box.service';
import {Text} from 'react-native-paper';
import Moment from 'react-moment';

interface BoxHistoricalScreenProps {
  navigation: NavigationStackProp;
}

const BoxHistoricalScreen: React.FC<BoxHistoricalScreenProps> = ({
  navigation,
}) => {
  const {params} = navigation.state;
  const [box, setBox] = useState<BoxModel>();
  const [expdCollect, setExpdCollect] = useState<boolean>();

  useEffect(() => {
    BoxService.getBox(params!.id).then(hBox => {
      setBox(hBox);
    });
  }, []);

  return (
    <View style={styles.screen}>
      {box ? (
        <View style={styles.content}>
          <View style={styles.header}>
            <View style={styles.row}>
              <Title style={styles.headerText}>{box.name}</Title>
              <Title style={styles.headerText}>
                {box && <Currency value={box.totalAmount} />}
              </Title>
            </View>
            <View style={styles.row}>
              <Text style={styles.headerText}>By</Text>
              <Text style={styles.headerText}>{box.createdBy.name}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.headerText}>Started At</Text>
              <Moment
                element={Text}
                format="D MMM YYYY"
                style={styles.headerText}>
                {box.createdAt}
              </Moment>
            </View>
            <View style={styles.row}>
              <Text style={styles.headerText}>Closed At</Text>
              <Moment
                element={Text}
                format="D MMM YYYY"
                style={styles.headerText}>
                {box.createdAt}
              </Moment>
            </View>
            {/* <View style={styles.row}>
              <Text style={styles.headerText}>Total Amount</Text>
              <Currency value={box.totalAmount} style={styles.headerText} />
            </View> */}
          </View>
          <ScrollView
            style={styles.details}
            showsVerticalScrollIndicator={false}>
            <View style={styles.margin}>
              <Title>Collection</Title>
              <CollectionList collection={box.collect!} />
            </View>
            <View style={styles.charges}>
              <Title style={styles.margin}>Charges</Title>
              {box.charges.map((charge: ChargeModel) => (
                <Charge charge={charge} key={charge.description} />
              ))}
            </View>
          </ScrollView>
        </View>
      ) : (
        <View style={styles.loading}>
          <Text>Loading...</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: color.background,
  },
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
  },
  header: {
    margin: spacing.small,
    padding: spacing.normal,
    borderBottomStartRadius: spacing.normal,
    borderTopStartRadius: spacing.normal,
    borderBottomEndRadius: spacing.normal,
    backgroundColor: color.primary,
  },
  headerText: {
    color: color.text,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: spacing.tiny,
  },
  margin: {
    margin: spacing.normal,
  },
  details: {
    // padding: spacing.normal,
    margin: spacing.small,
    marginBottom: spacing.none,
    borderTopLeftRadius: spacing.normal,
    borderTopRightRadius: spacing.normal,
    backgroundColor: color.line,
  },
  charges: {
    marginBottom: spacing.huge,
  },
});

export default BoxHistoricalScreen;
