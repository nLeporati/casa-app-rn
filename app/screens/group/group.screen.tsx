import React, {useState} from 'react';
import {
  Title,
  Button,
  Caption,
  TextInput,
  ActivityIndicator,
} from 'react-native-paper';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import {color, spacing} from '../../theme';
import {GroupModel} from '../../models/group.model';
import GroupMember from '../../components/group-member';
import NewGroupMember from '../../components/new-group-member';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../hooks/use-stores';
import {UserModel} from '../../models/user.model';
import UserService from '../../services/user.service';
import GroupService from '../../services/group.service';

type GroupScreenProps = {};
type GroupScreenState = {
  group: GroupModel;
  isAdding: boolean;
};

const GroupScreen: React.FC<GroupScreenProps> = observer(() => {
  const {groupStore, sessionStore} = useStores();
  const [memberName, setMemberName] = useState<string>('');
  const [isAdding, setIsAdding] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const loadGroup = async () => {
    await groupStore.loadGroup(sessionStore.user!.groups[0]);
  };

  const toggleAdding = () => {
    setIsAdding(!isAdding);
    setIsLoading(false);
  };

  const addNewMember = async () => {
    setIsLoading(true);
    const user = await UserService.createUser(memberName).then();
    if (user) {
      await GroupService.addMember(groupStore.group!._id, user._id);
    }
    await loadGroup();
    toggleAdding();
    setIsLoading(false);
  };

  const deleteMember = async (userId: string) => {
    await GroupService.removeMemeber(groupStore.group!._id, userId);
    loadGroup();
  };

  const editMember = async (user: UserModel) => {
    await UserService.updateUser(user);
    loadGroup();
  };

  return (
    <View style={styles.screen}>
      <View style={styles.header}>
        <Title style={styles.title}>Members</Title>
        {isAdding && (
          <View style={styles.buttons}>
            <Button
              onPress={() => toggleAdding()}
              color={color.error}
              disabled={isLoading}>
              Cancel
            </Button>
            <Button
              onPress={() => addNewMember()}
              color={color.primary}
              disabled={!(memberName.length > 3 && !isLoading)}
              mode="contained">
              Save
            </Button>
          </View>
        )}
        {!isAdding && (
          <Button onPress={() => toggleAdding()} color={color.primary}>
            Add Member
          </Button>
        )}
      </View>
      <View style={styles.header}>
        <Caption>INVITE CODE</Caption>
        <Caption>{groupStore.group?.inviteCode}</Caption>
      </View>
      {isAdding && (
        <View>
          <TextInput
            mode="outlined"
            label="New Member Name"
            disabled={isLoading}
            onChange={({nativeEvent}) =>
              setMemberName(nativeEvent.text)
            }></TextInput>
        </View>
      )}
      {!isAdding && (
        <ScrollView style={styles.scroll}>
          {groupStore.group?.users.map(user => {
            return (
              <GroupMember
                key={user._id}
                user={user}
                onDelete={(id: string) => deleteMember(id)}
                onEditing={(userEdited: UserModel) => editMember(userEdited)}
              />
            );
          })}
        </ScrollView>
      )}
      {isLoading && (
        <ActivityIndicator
          size="large"
          color={color.primary}
          style={{flex: 1, justifyContent: 'center'}}
        />
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: color.background,
    padding: spacing.normal,
    paddingBottom: spacing.none,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttons: {
    flexDirection: 'row',
  },
  title: {
    fontSize: spacing.large,
  },
  scroll: {
    minHeight: 80,
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: spacing.normal,
  },
  userName: {
    marginLeft: spacing.normal,
  },
});

export default GroupScreen;
