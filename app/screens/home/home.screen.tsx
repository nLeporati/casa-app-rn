import React, {Component, ContextType} from 'react';
import {StyleSheet, View, TouchableWithoutFeedback} from 'react-native';
import {
  Text,
  TextInput,
  Button,
  ActivityIndicator,
  Switch,
} from 'react-native-paper';
import {color, spacing} from '../../theme';
import UserService from '../../services/user.service';
import GroupService from '../../services/group.service';
import {NavigationStackProp} from 'react-navigation-stack';
import storesContext from '../../contexts/storesContext';
import AuthService from '../../services/auth.service';
import {AccountModel} from '../../models/account.model';
import {UserModel} from '../../models/user.model';
import {GroupModel} from '../../models/group.model';

type HomeScreenProps = {
  navigation: NavigationStackProp;
};

type HomeScreenState = {
  step: number;
  loading: boolean;
  userName: string;
  groupName: string;
  joinGroup: boolean;
  inviteCode: string;
};

class HomeScreen extends Component<HomeScreenProps, HomeScreenState> {
  constructor(props: any) {
    super(props);
    this.initState();
  }

  declare context: ContextType<typeof storesContext>;
  static contextType = storesContext;

  initState(): void {
    this.state = {
      step: 0,
      loading: false,
      userName: '',
      groupName: '',
      joinGroup: false,
      inviteCode: '',
    };
  }

  start(): void {
    if (this.state.step === 0) {
      this.changeStep(1);
    }
  }

  changeStep(step: number): void {
    if (this.state.step === 2 && step != -1) {
      this.createAsync();
    } else {
      this.setState({
        step: this.state.step + step,
      });
    }
  }

  async createAsync() {
    this.setState({step: 0, loading: true});
    const user = await UserService.createUser(this.state.userName);
    if (user) {
      await this.registerAsync(user);
    }
    this.setState({loading: false});
  }

  async registerAsync(user: UserModel): Promise<any> {
    const account: AccountModel = {
      user: user._id,
      username: user._id,
      password: user._id.slice(0, 7),
    };
    try {
      await AuthService.signup(account);
      await this.context.sessionStore.setAccount(account);
      await this.context.sessionStore.login();
      let group: GroupModel | undefined;
      if (this.state.joinGroup) {
        group = await this.joinGroupAsync(user);
      } else {
        group = await this.createGroupAsync(user);
      }
      this.context.sessionStore.user!.groups.push(group!._id);
      this.props.navigation.navigate('AuthLoading');
    } catch (error) {
      this.initState();
    }
  }

  async createGroupAsync(user: UserModel): Promise<GroupModel | undefined> {
    return await GroupService.createGroup(this.state.groupName, user._id);
  }

  async joinGroupAsync(user: UserModel): Promise<GroupModel | undefined> {
    return await GroupService.joinGroup(
      user._id,
      this.state.inviteCode.toUpperCase(),
    );
  }

  render() {
    return (
      <View style={styles.screen}>
        <View>
          {this.state.step === 0 && (
            <TouchableWithoutFeedback onPress={() => this.start()}>
              <Text style={styles.title}>TOUCH TO BOXER</Text>
            </TouchableWithoutFeedback>
          )}
          {this.state.step === 1 && (
            <Text style={styles.title}>CREATE AN ACCOUNT</Text>
          )}
          {this.state.step === 2 && (
            <Text style={styles.title}>SELECT A GROUP</Text>
          )}
        </View>
        {!this.state.loading && (
          <>
            {this.state.step === 1 && (
              <View style={styles.input}>
                <TextInput
                  mode="outlined"
                  label="Your Name"
                  onChange={({nativeEvent}) =>
                    this.setState({userName: nativeEvent.text})
                  }></TextInput>
              </View>
            )}
            {this.state.step === 2 && (
              <View style={styles.input}>
                <View style={styles.buttons}>
                  <Text>Join to a Group</Text>
                  <Switch
                    value={this.state.joinGroup}
                    color={color.primary}
                    onValueChange={() =>
                      this.setState({joinGroup: !this.state.joinGroup})
                    }></Switch>
                </View>
                {!this.state.joinGroup ? (
                  <TextInput
                    mode="outlined"
                    label="Group Name"
                    value={this.state.groupName}
                    onChange={({nativeEvent}) =>
                      this.setState({groupName: nativeEvent.text})
                    }></TextInput>
                ) : (
                  <TextInput
                    mode="outlined"
                    label="Invite Code"
                    value={this.state.inviteCode}
                    onChange={({nativeEvent}) =>
                      this.setState({inviteCode: nativeEvent.text})
                    }></TextInput>
                )}
              </View>
            )}
            {this.state.step > 0 && (
              <View style={styles.input}>
                <View style={styles.buttons}>
                  <View style={styles.button}>
                    <Button
                      mode="contained"
                      color={color.text}
                      onPress={() => this.changeStep(-1)}>
                      Back
                    </Button>
                  </View>
                  <View style={styles.button}>
                    <Button mode="contained" onPress={() => this.changeStep(1)}>
                      Next
                    </Button>
                  </View>
                </View>
              </View>
            )}
          </>
        )}

        {this.state.loading && (
          <ActivityIndicator size="large" color={color.primary} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: spacing.small,
  },
  title: {
    // flex: 1,
    // justifyContent: 'center',
    // alignSelf: 'center',
    marginBottom: spacing.medium,
    fontSize: 40,
    textAlign: 'center',
    color: color.primary,
  },
  input: {
    width: '80%',
    alignSelf: 'center',
    margin: spacing.tiny,
  },
  buttons: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    flex: 0.48,
    // margin: spacing.tiny,
    // width: '45%',
    // height: '100%',
  },
});

export default HomeScreen;
