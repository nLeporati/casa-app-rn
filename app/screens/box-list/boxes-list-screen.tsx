import React, {ContextType} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {Text, Title} from 'react-native-paper';
import {spacing, color} from '../../theme';
import BoxFAB from '../../components/box-fab';
import {NavigationStackProp} from 'react-navigation-stack';
import {BoxModel} from '../../models/box.model';
import BoxListItem from '../../components/box-list-item';
import BoxService from '../../services/box.service';
import storesContext from '../../contexts/storesContext';

type BoxesListScreenProps = {
  navigation: NavigationStackProp;
};

type BoxesListScreenState = {
  boxesList: BoxModel[];
};

class BoxesListScreen extends React.Component<
  BoxesListScreenProps,
  BoxesListScreenState
> {
  constructor(props: BoxesListScreenProps) {
    super(props);
    this.state = {
      boxesList: new Array(),
    };
  }

  declare context: ContextType<typeof storesContext>;
  static contextType = storesContext;

  componentDidMount() {
    BoxService.getHistorical(this.context.sessionStore.user!._id).then(list => {
      this.setState({boxesList: list});
    });
  }

  render() {
    return (
      <View style={styles.screen}>
        <View>
          <View style={styles.header}>
            <Title style={styles.title}>Boxes Historial</Title>
          </View>

          {this.state.boxesList.length > 0 ? (
            <ScrollView style={styles.boxes}>
              {this.state.boxesList.map((box: BoxModel) => {
                return (
                  <BoxListItem
                    box={box}
                    key={box._id}
                    navigation={this.props.navigation}
                  />
                );
              })}
            </ScrollView>
          ) : (
            <View style={styles.empty}>
              <Text style={{color: color.dim}}>empty historial</Text>
            </View>
          )}
        </View>

        {/* <BoxFAB navigation={this.props.navigation} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingVertical: spacing.large,
    backgroundColor: color.background,
  },
  title: {
    fontSize: spacing.large,
  },
  header: {
    marginBottom: spacing.large,
    paddingHorizontal: spacing.normal,
  },
  boxes: {
    marginBottom: spacing.normal,
  },
  empty: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 200,
  },
});

export default BoxesListScreen;
