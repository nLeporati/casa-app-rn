import React, {ContextType} from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {NavigationSwitchProp} from 'react-navigation';
import {color} from '../../theme';
import storesContext from '../../contexts/storesContext';
import AuthService from '../../services/auth.service';

type AuthLoadingScreenProps = {
  navigation: NavigationSwitchProp;
};

class AuthLoadingScreen extends React.Component<AuthLoadingScreenProps> {
  declare context: ContextType<typeof storesContext>;
  static contextType = storesContext;

  componentDidMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const account = await this.context.sessionStore.getAccount();
    if (!this.context.sessionStore.jwtToken && account) {
      await this.context.sessionStore.login();
    }

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(
      this.context.sessionStore.user ? 'App' : 'Auth',
    );
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator size="large" color={color.primary} />
      </View>
    );
  }
}

export default AuthLoadingScreen;
