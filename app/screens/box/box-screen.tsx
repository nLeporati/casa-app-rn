import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  List,
  Colors,
  Title,
  Button,
  Text,
  Snackbar,
  ActivityIndicator,
} from 'react-native-paper';
import Modal from 'react-native-modal';
import Charge from '../../components/charge';
import BoxFAB from '../../components/box-fab';
import {color} from '../../theme/color';
import Currency from '../../components/currency';
import AddChargeModal from '../../components/add-charge-modal';
import DATA_GROUPS from '../../data/data-groups';
import {ChargeModel} from '../../models/charge.model';
import CollectModal from '../../components/collect-modal';
import {GroupModel} from '../../models/group.model';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../hooks/use-stores';
import {NavigationStackProp} from 'react-navigation-stack';
import DeleteChargeDialog from './delete-charge-dialog';
import {spacing} from '../../theme';
import {BoxModel} from '../../models/box.model';
import BoxService from '../../services/box.service';
import * as storage from '../../utils/storage';
import {UserModel} from '../../models/user.model';

type BoxScreenProps = {
  navigation: NavigationStackProp;
};

const BoxScreen: React.FC<BoxScreenProps> = observer(({navigation}) => {
  const {groupStore, sessionStore} = useStores();
  const [box, setBox] = useState<BoxModel | undefined>(undefined);
  const [selectedCharge, setSelectedCharge] = useState<ChargeModel | undefined>(
    undefined,
  );
  const [addChargeModalIsOpen, setAddChargeModalIsOpen] = useState<boolean>(
    false,
  );
  const [collectModalIsOpen, setCollectModalIsOpen] = useState<boolean>(false);
  const [deleteChargeModalIsOpen, setDeleteChargeModalIsOpen] = useState<
    boolean
  >(false);

  useEffect(() => {
    if (sessionStore.user != null) {
      loadGroup();
    } else {
      navigation.navigate('AuthLoading');
    }
  }, []);

  const loadGroup = async () => {
    groupStore.loadGroup(sessionStore.user!.groups[0]).then(() => {
      getActiveBox();
    });
  };

  const getActiveBox = async () => {
    if (groupStore.group?.activeBox != null) {
      const activeBox = await BoxService.getBox(groupStore.group?.activeBox);
      if (activeBox) setBox(activeBox);
    }
  };

  // Delete Charge
  const onChargeLongPress = (charge: ChargeModel) => {
    setSelectedCharge(charge);
    setDeleteChargeModalIsOpen(true);
  };

  const onDeleteCharge = async () => {
    if (selectedCharge) {
      await groupStore.removeCharge(selectedCharge._id!);
      await getActiveBox();
    }
    setSelectedCharge(undefined);
    setDeleteChargeModalIsOpen(false);
  };
  // -------------

  const onAddChargeSuccess = async () => {
    await getActiveBox();
    setAddChargeModalIsOpen(false);
  };

  const onCreateBox = () => {
    groupStore.group!.activeBox = 'creating';
    const box = {
      name: 'demo',
      createdBy: sessionStore.user!._id,
      group: sessionStore.user?.groups[0],
    };
    BoxService.createBox(box)
      .then(() => {
        loadGroup();
      })
      .catch(() => {
        groupStore.group!.activeBox = undefined;
      });
  };

  const onCollectSuccess = async () => {
    setBox(undefined);
    setCollectModalIsOpen(false);
  };

  const deleteSession = () => {
    sessionStore.removeSession();
    navigation.navigate('Home');
  };

  return (
    <View style={styles.screen}>
      <View style={styles.header}>
        <View style={styles.title}>
          <TouchableWithoutFeedback onLongPress={() => deleteSession()}>
            <Title style={styles.headerText}>{groupStore.group?.name}</Title>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.totalCharge}>
          <Title style={styles.headerText}>
            {box && <Currency value={box.totalAmount} />}
          </Title>
        </View>
      </View>

      {groupStore.group && groupStore.group.activeBox == null && (
        <View style={styles.center}>
          <Text
            style={{
              color: color.dim,
              marginBottom: spacing.normal,
              fontSize: 48,
            }}>
            No active Box
          </Text>
          <Button mode="outlined" onPress={() => onCreateBox()}>
            Start a Box
          </Button>
        </View>
      )}

      {(groupStore.group == null ||
        (groupStore.group == null && box == null)) && (
        <ActivityIndicator
          size="large"
          color={color.primary}
          style={{flex: 1, justifyContent: 'center'}}
        />
      )}

      {box != null && (
        <View style={{flex: 1}}>
          <View style={styles.actionBar}>
            <Button onPress={() => setAddChargeModalIsOpen(true)}>
              + Charge
            </Button>
            {box.charges.length > 0 && (
              <Button onPress={() => setCollectModalIsOpen(true)}>
                Collect #
              </Button>
            )}
          </View>

          <ScrollView style={styles.charges}>
            {box.charges.map((charge: ChargeModel) => (
              <Charge
                charge={charge}
                key={charge._id}
                onLongPress={() => onChargeLongPress(charge)}
              />
            ))}
          </ScrollView>

          <Modal
            isVisible={addChargeModalIsOpen}
            onBackdropPress={() => setAddChargeModalIsOpen(false)}>
            <AddChargeModal onSuccess={() => onAddChargeSuccess()} />
          </Modal>

          <DeleteChargeDialog
            visible={deleteChargeModalIsOpen}
            onConfirm={() => onDeleteCharge()}
            onCancel={() => setDeleteChargeModalIsOpen(false)}
          />

          <Modal
            isVisible={collectModalIsOpen}
            onDismiss={() => console.log('dismiss')}
            onBackButtonPress={() => setCollectModalIsOpen(false)}
            onBackdropPress={() => setCollectModalIsOpen(false)}>
            <CollectModal onSuccess={() => onCollectSuccess()} />
          </Modal>
        </View>
      )}

      <BoxFAB navigation={navigation} />
    </View>
  );
});

const styles = StyleSheet.create({
  test: {
    minHeight: 600,
    flex: 1,
  },
  screen: {
    backgroundColor: color.background,
    flex: 1,
  },
  header: {
    backgroundColor: color.primary,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 70,
    margin: 5,
    marginTop: 15,
    padding: 20,
    borderRadius: 16,
  },
  disabled: {
    backgroundColor: color.disabled,
  },
  headerText: {
    color: Colors.white,
  },
  title: {},
  totalCharge: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  actionBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  charges: {
    marginBottom: spacing.large,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 80,
  },
});

export default BoxScreen;
