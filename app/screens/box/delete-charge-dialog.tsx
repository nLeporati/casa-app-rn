import React, {useState} from 'react';
import {Portal, Dialog, Paragraph, Button, Text} from 'react-native-paper';
import {color} from '../../theme';
import {StyleSheet} from 'react-native';

type DialogProps = {
  visible: boolean;
  onConfirm: () => void;
  onCancel: () => void;
};

const DeleteChargeDialog: React.FC<DialogProps> = ({
  onConfirm,
  onCancel,
  visible,
}) => {
  const [isVisble, setIsVisible] = useState<boolean>(false);

  return (
    <Portal>
      <Dialog visible={visible} onDismiss={() => onCancel()}>
        <Dialog.Title>Remove Charge</Dialog.Title>
        <Dialog.Content>
          <Paragraph>Are you sure to remove this charge?</Paragraph>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={() => onCancel()}>
            <Text style={styles.dialogCancel}>Cancel</Text>
          </Button>
          <Button onPress={() => onConfirm()}>Confirm</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

const styles = StyleSheet.create({
  dialogCancel: {
    color: color.dim,
  },
});

export default DeleteChargeDialog;
