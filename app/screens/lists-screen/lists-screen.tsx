import React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {List} from 'react-native-paper';
import {color} from '../../theme/color';

const ListsScreen = () => {
  return (
    <View>
      <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
        <View style={styles.body}>
          <List.Section>
            <List.Subheader>Casa</List.Subheader>
            <List.Item title="Pan" />
            <List.Item title="Huevos" />
            <List.Item title="Leche" />
          </List.Section>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: color.background,
  },
  body: {
    backgroundColor: color.background,
  },
});

export default ListsScreen;
