import {decorate, observable, action} from 'mobx';
import {AccountModel} from '../models/account.model';
import {UserModel} from '../models/user.model';
import {AuthModel} from '../models/auth.model';
import AuthService from '../services/auth.service';
var jwtDecode = require('jwt-decode');
import * as storage from '../utils/storage';

export class SessionStore {
  user: UserModel | null = null;
  jwtToken: string | null = null;
  account: AccountModel | null = null;

  login = async (): Promise<void> => {
    const token = await AuthService.login(this.account!);
    console.log('jwtToken', token);
    await this.setJwtToken(token!);
  };

  setJwtToken = async (jwtToken: string): Promise<boolean> => {
    this.jwtToken = jwtToken;
    this.setUser();
    return await storage.saveString('jwtToken', this.jwtToken);
  };

  setUser = (): void => {
    const auth = jwtDecode(this.jwtToken) as AuthModel;
    this.user = auth.user;
  };

  // getJwtToken = async (): Promise<string | null> => {
  //   const jwtToken = await storage.loadString('jwtToken');
  //   this.jwtToken = jwtToken;
  //   return this.jwtToken;
  // };

  setAccount = async (account: AccountModel): Promise<boolean> => {
    this.account = account;
    return await storage.save('account', this.account);
  };

  getAccount = async (): Promise<AccountModel | null> => {
    const account = await storage.load('account');
    this.account = account;
    return this.account;
  };

  removeSession = async (): Promise<void> => {
    this.account = null;
    await storage.clear();
  };
}

decorate(SessionStore, {
  user: observable,
  account: observable,
  setUser: action,
  setJwtToken: action,
  getJwtToken: action,
  setAccount: action,
  getAccount: action,
  removeSession: action,
});
