import {decorate, observable, action, computed} from 'mobx';
import {GroupModel} from '../models/group.model';
import DATA_GROUPS from '../data/data-groups';
import {ChargeModel} from '../models/charge.model';
import {BoxModel} from '../models/box.model';
import {UserModel} from '../models/user.model';
import GroupService from '../services/group.service';
import BoxService from '../services/box.service';
import ChargeService from '../services/charge.service';

export class GroupStore {
  // observables
  group: GroupModel | undefined = undefined;

  // computeds
  get activeBox() {
    return this.group?.activeBox;
  }

  // actions
  getMember = (id: string): UserModel | undefined => {
    return this.group!.users.find(user => user._id == id);
  };

  loadGroup = (id: string): Promise<void> => {
    return GroupService.getGroup(id)
      .then(group => {
        if (group != null) {
          this.group = group;
        }
      })
      .catch(error => {
        console.log('group error 1', error);
        this.group = undefined;
      });
  };

  setGroupActiveBox = async (boxId: string | undefined): Promise<void> => {
    await GroupService.updateActiveGroup(this.group!._id, boxId).then(() => {
      this.group!.activeBox = boxId;
    });
  };

  addCharge = async (charge: ChargeModel): Promise<void> => {
    await ChargeService.createCharge(charge);
  };

  removeCharge = async (id: string): Promise<void> => {
    await ChargeService.deleteCharge(id);
  };

  updateMember = (user: UserModel) => {
    this.group!.users[Number(user._id)].name = user.name;
    this.group!.users[Number(user._id)].avatarText = user.name
      .slice(0, 2)
      .toUpperCase();
  };
}

// another way to decorate variables with observable
decorate(GroupStore, {
  group: observable,
  activeBox: computed,
  getMember: action,
  loadGroup: action,
  setGroupActiveBox: action,
  addCharge: action,
  removeCharge: action,
  updateMember: action,
});
