import DATA_USERS from './data-users';
import {CollectionModel, Collection} from '../models/collection-model';

export const DATA_COLLECTIONS: any = [
  {
    collector: DATA_USERS[0],
    totalAmount: 4500,
    charges: [
      {payer: DATA_USERS[1], amount: 3500},
      {payer: DATA_USERS[2], amount: 1000},
    ],
  },
  {
    collector: DATA_USERS[1],
    totalAmount: 0,
    charges: [
      {payer: DATA_USERS[0], amount: 0},
      {payer: DATA_USERS[2], amount: 0},
    ],
  },
  {
    collector: DATA_USERS[2],
    totalAmount: 2500,
    charges: [
      {payer: DATA_USERS[0], amount: 0},
      {payer: DATA_USERS[1], amount: 2500},
    ],
  },
];
