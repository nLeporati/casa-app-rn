import {UserModel} from '../models/user.model';

const DATA_USERS: UserModel[] = [
  {_id: '0', name: 'Nicolás Leporati', avatarText: 'NL'},
  {_id: '1', name: 'Jenny Saez', avatarText: 'JS'},
  {_id: '2', name: 'Kasandra Duran', avatarText: 'KD'},
];

export default DATA_USERS;
