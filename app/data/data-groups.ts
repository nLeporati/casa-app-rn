import DATA_USERS from './data-users';
import DATA_BOXES from './data-boxes';
import {GroupModel} from '../models/group.model';

const DATA_GROUPS: GroupModel[] = [
  {
    _id: '1',
    name: 'My Group',
    users: DATA_USERS,
    boxes: DATA_BOXES,
    activeBox: DATA_BOXES[0],
    ceratedBy: '0',
    createdAt: '2020-02-02',
  },
];

export default DATA_GROUPS;
