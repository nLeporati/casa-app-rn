import DATA_USERS from './data-users';
import {ChargeModel} from '../models/charge.model';

const DATA_CHARGES: ChargeModel[] = [
  {
    _id: '0',
    description: 'Feria',
    amount: 3000,
    payer: DATA_USERS[0],
    box: '1',
    createdAt: '2020-05-01',
  },
  {
    _id: '1',
    description: 'Unimarc',
    amount: 13500,
    payer: DATA_USERS[1],
    box: '1',
    createdAt: '2020-05-01',
  },
  {
    _id: '2',
    description: 'Agua',
    amount: 6000,
    payer: DATA_USERS[2],
    box: '1',
    createdAt: '2020-05-01',
  },
];

export default DATA_CHARGES;
