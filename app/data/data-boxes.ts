import DATA_CHARGES from './data-charges';
import DATA_USERS from './data-users';
import {BoxModel} from '../models/box.model';

const DATA_BOXES: BoxModel[] = [
  {
    _id: '0',
    name: 'Prueba 5 de Enero',
    totalAmount: 19500,
    createdAt: '2020/01/26',
    createdBy: DATA_USERS[0],
    charges: DATA_CHARGES,
    group: '1',
  },
  {
    _id: '1',
    name: 'Semana 2 de Marzo',
    totalAmount: 390000,
    createdAt: '2019/12/31',
    createdBy: DATA_USERS[1],
    charges: DATA_CHARGES,
    group: '1',
  },
];

export default DATA_BOXES;
