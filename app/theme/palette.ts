import {Colors} from 'react-native-paper';

export const palette = {
  black: '#1d1d1d',
  white: '#ffffff',
  offWhite: '#e6e6e6',
  orange: '#FBA928',
  orangeDarker: '#EB9918',
  lightGrey: '#939AA4',
  lighterGrey: '#CDD4DA',
  angry: '#dd3333',
  deepPurpe: '#7e57c2',
  deepPurpeDark: '#4d2c91',
  deepPurpeLight: '#b085f5',
  green: Colors.lightGreen400,
  amber: Colors.deepPurple100,
};
