import React, {useState, useEffect} from 'react';
import {Title, Avatar, TextInput, Button, Colors} from 'react-native-paper';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import {spacing} from '../theme/spacing';
import {color} from '../theme/color';
import DATA_USERS from '../data/data-users';
import {useStores} from '../hooks/use-stores';
import {observer} from 'mobx-react-lite';
import {ChargeModel} from '../models/charge.model';

interface ModalProps {
  onSuccess: Function;
}

const AddChargeModal: React.FC<ModalProps> = observer(({onSuccess}) => {
  const {groupStore} = useStores();

  const [selected, setSelected] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [amount, setAmount] = useState<string>('');
  const [formIsValid, setFormIsValid] = useState<boolean>(false);

  const addCharge = async () => {
    setFormIsValid(false);
    const charge: ChargeModel = {
      payer: groupStore.getMember(selected)!,
      description,
      amount: +amount,
      box: groupStore.group!.activeBox!,
    };
    await groupStore.addCharge(charge);
    onSuccess();
  };

  useEffect(() => {
    const isValid = selected && description && amount ? true : false;
    setFormIsValid(isValid);
  }, [selected, description, amount]);

  return (
    <View style={styles.content}>
      <View style={styles.title}>
        <Title>Add a Charge</Title>
      </View>
      <Text>Payer</Text>
      <ScrollView style={styles.payers} horizontal>
        {groupStore.group?.users.map(payer => {
          return (
            <View
              key={payer.avatarText}
              style={[
                payer._id === selected ? styles.selected : styles.noSelected,
              ]}>
              <TouchableHighlight
                onPress={() => setSelected(payer._id)}
                underlayColor={color.transparent}>
                <Avatar.Text
                  label={payer.avatarText}
                  size={64}
                  style={styles.payer}
                />
              </TouchableHighlight>
            </View>
          );
        })}
      </ScrollView>

      <Text>{selected && groupStore.getMember(selected)?.name}</Text>

      <View style={styles.amount}>
        <TextInput
          label="Description"
          value={description}
          onChangeText={text => setDescription(text)}
        />
      </View>
      <View style={styles.amount}>
        <TextInput
          label="Amount"
          mode="outlined"
          keyboardType="number-pad"
          value={amount}
          onChangeText={text => setAmount(text)}
        />
      </View>
      <View style={styles.addButton}>
        <Button
          mode="contained"
          onPress={() => addCharge()}
          disabled={!formIsValid}>
          Save
        </Button>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.background,
    margin: spacing.normal,
    padding: spacing.normal,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: spacing.tiny,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  title: {
    marginBottom: spacing.small,
    margin: spacing.small,
  },
  payersTitle: {
    textAlign: 'left',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  payers: {
    margin: spacing.small,
    minHeight: 80,
  },
  payer: {
    margin: spacing.tiny,
  },
  amount: {
    margin: spacing.tiny,
    height: 70,
    width: '100%',
  },
  addButton: {
    margin: spacing.small,
    width: '100%',
  },
  noSelected: {
    backgroundColor: color.transparent,
    borderRadius: 50,
    margin: spacing.tiny,
  },
  selected: {
    backgroundColor: Colors.lightGreen400,
    borderRadius: 50,
    margin: spacing.tiny,
  },
});

export default AddChargeModal;
