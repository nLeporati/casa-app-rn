import React from 'react';
import {Button, Dialog} from 'react-native-paper';

type ConfirmDialogProps = {
  text: string;
  onConfirm: Function;
  onCancel: Function;
};

class ConfirmDialog extends React.Component {
  render() {
    return (
      <Dialog visible={this.state.visible} onDismiss={this._hideDialog}>
        <Dialog.Title>Alert</Dialog.Title>
        <Dialog.Content>
          <Paragraph>This is simple dialog</Paragraph>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={this._hideDialog}>Done</Button>
        </Dialog.Actions>
      </Dialog>
    );
  }
}
