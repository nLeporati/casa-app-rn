/* eslint-disable react/prefer-stateless-function */
import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Title, Text} from 'react-native-paper';

class DefaultModal extends Component {
  render() {
    return <View style={styles.content}>{this.renderContent()}</View>;
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
});

export default DefaultModal;
