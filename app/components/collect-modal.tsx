import React, {useState, useEffect} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {color, spacing} from '../theme';
import {
  Title,
  Text,
  Caption,
  Button,
  ActivityIndicator,
  Subheading,
} from 'react-native-paper';
import {DATA_COLLECTIONS} from '../data/data-collections';
import {useStores} from '../hooks/use-stores';
import {Collection} from '../models/collection-model';
import {CollectModel} from '../models/collect.model';
import CollectionList from './collection-list';
import BoxService from '../services/box.service';

interface ICollectModalProps {
  onSuccess(): void;
}

const CollectModal: React.FC<ICollectModalProps> = ({onSuccess}) => {
  const [collections, setCollections] = useState<CollectModel[]>(
    new Array<CollectModel>(),
  );
  const {groupStore} = useStores();

  const collect = async () => {
    await BoxService.confirmCollect(groupStore.group!.activeBox!);
    groupStore.group!.activeBox = undefined;
    onSuccess();
  };

  useEffect(() => {
    BoxService.collect(groupStore.group!.activeBox!)
      .then(data => {
        setCollections(data!);
      })
      .catch(error => {
        console.log('collect error', error);
      });
  }, []);

  return (
    <View style={styles.content}>
      <View style={styles.title}>
        <Title>Collect Charges</Title>
      </View>

      {collections.length > 0 ? (
        <View>
          <View style={styles.description}>
            <Subheading>Persona</Subheading>
            <Subheading>Paga a</Subheading>
          </View>
          <ScrollView style={styles.scroll}>
            {collections
              .filter(c => c.payments.length > 0)
              .map(collection => {
                return (
                  <View style={styles.collect} key={collection.collector.name}>
                    <View style={styles.collector}>
                      <Text>{collection.collector.name}</Text>
                    </View>

                    <View style={styles.charges}>
                      <View style={styles.names}>
                        {collection.payments.map(payment => (
                          <Caption key={payment.user.name}>
                            {payment.user.name}
                          </Caption>
                        ))}
                      </View>
                      <View style={styles.amount}>
                        {collection.payments.map(payment => (
                          <Caption key={'amount_' + payment.user.name}>
                            $ {payment.amount}
                          </Caption>
                        ))}
                      </View>
                    </View>
                  </View>
                );
              })}
          </ScrollView>

          <View style={styles.addButton}>
            <Button mode="contained" icon="hand-okay" onPress={() => collect()}>
              Collected!
            </Button>
          </View>
        </View>
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator
            size="small"
            color={color.primary}
            style={{flex: 1, justifyContent: 'center'}}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: color.background,
    margin: spacing.normal,
    padding: spacing.normal,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: spacing.tiny,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  title: {
    marginBottom: spacing.small,
    margin: spacing.small,
  },
  scroll: {
    maxHeight: 250,
  },
  loader: {
    minHeight: 150,
  },
  description: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  charges: {
    flexDirection: 'row',
    minWidth: '100%',
    alignItems: 'flex-start',
  },
  collect: {
    marginBottom: spacing.normal,
  },
  collector: {
    // margin: spacing.normal,
    // flex: 2,
  },
  names: {
    flex: 1,
  },
  amounts: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    textAlign: 'right',
  },
  amount: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    textAlign: 'right',
  },
  addButton: {
    margin: spacing.small,
    width: '100%',
    alignSelf: 'center',
  },
});

export default CollectModal;
