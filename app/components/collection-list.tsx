import React from 'react';
import {View, ScrollView, StyleSheet, Text} from 'react-native';
import {Caption} from 'react-native-paper';
import {spacing} from '../theme';
import {CollectModel} from '../models/collect.model';

interface CollectionListProps {
  collection: CollectModel[];
}

const CollectionList: React.FC<CollectionListProps> = ({collection}) => {
  return (
    <View>
      {collection
        .filter(c => c.payments.length > 0)
        .map(coll => {
          return (
            <View style={styles.collect} key={coll.collector.name}>
              <View style={styles.collector}>
                <Text>{coll.collector.name}</Text>
              </View>

              <View style={styles.chargescoll}>
                <View style={styles.names}>
                  {coll.payments
                    .filter(pay => pay.amount > 0)
                    .map(payment => (
                      <Caption key={'charge_' + payment.user.name}>
                        {payment.user.name}
                      </Caption>
                    ))}
                </View>
                <View style={styles.amount}>
                  {coll.payments
                    .filter(pay => pay.amount > 0)
                    .map(payment => (
                      <Caption key={'amount_' + payment.user.name}>
                        $ {payment.amount}
                      </Caption>
                    ))}
                </View>
              </View>
            </View>
          );
        })}
    </View>
  );
};

const styles = StyleSheet.create({
  scroll: {
    maxHeight: 250,
  },
  chargescoll: {
    flexDirection: 'row',
    minWidth: '100%',
    alignItems: 'flex-start',
  },
  collect: {
    marginBottom: spacing.normal,
  },
  collector: {
    // margin: spacing.normal,
    // flex: 2,
  },
  names: {
    flex: 1,
  },
  amounts: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    textAlign: 'right',
  },
  amount: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    textAlign: 'right',
  },
});

export default CollectionList;
