import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Caption, TouchableRipple} from 'react-native-paper';
import {color} from '../theme/color';
import {ChargeModel} from '../models/charge.model';
import {spacing} from '../theme';
import Moment from 'react-moment';

interface ChargeProps {
  charge: ChargeModel;
  onPress?: () => void | undefined;
  onLongPress?: () => void | undefined;
}

const Charge: React.FC<ChargeProps> = ({charge, onLongPress, onPress}) => {
  const onLongPressAction = () => {
    if (onLongPress) onLongPress();
  };
  const onPressAction = () => {
    if (onPress) onPress();
  };

  return (
    <TouchableRipple
      onLongPress={() => onLongPressAction()}
      onPress={onPress || onLongPress ? () => onPressAction() : undefined}>
      <View style={styles.charge}>
        <View style={styles.firstRow}>
          <View style={styles.left}>
            <Text style={styles.text}>{charge.description}</Text>
          </View>
          <View style={styles.right}>
            <Text style={styles.text}>$ {charge.amount}</Text>
          </View>
        </View>
        <View style={styles.details}>
          <Caption>{charge.payer?.name}</Caption>
          <Moment element={Caption} format="D MMM YYYY">
            {charge.updatedAt}
          </Moment>
        </View>
      </View>
    </TouchableRipple>
  );
};

const styles = StyleSheet.create({
  charge: {
    margin: spacing.normal,
    //paddingBottom: spacing.normal,
  },
  firstRow: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontSize: 18,
  },
  icon: {
    marginRight: spacing.small,
    backgroundColor: color.dim,
  },
  left: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',
  },
  details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Charge;
