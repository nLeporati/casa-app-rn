import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Caption, TouchableRipple} from 'react-native-paper';
import {BoxModel} from '../models/box.model';
import {color, spacing} from '../theme';
import {NavigationStackProp} from 'react-navigation-stack';
import Moment from 'react-moment';

type BoxListItemProps = {
  box: BoxModel;
  navigation: NavigationStackProp;
};

const BoxListItem: React.FC<BoxListItemProps> = ({box, navigation}) => {
  console.log(box);
  const selectBox = () => {
    console.log('CAMBIO');
    navigation.navigate('BoxHistorical', {id: box._id});
  };

  return (
    <TouchableRipple onPress={() => selectBox()}>
      <View style={styles.charge}>
        <View style={styles.firstRow}>
          <View style={styles.left}>
            <Text style={styles.text}>{box.name}</Text>
            <Moment element={Text} format="D MMM YYYY" style={styles.date}>
              {box.createdAt}
            </Moment>
          </View>
          <View style={styles.right}>
            <Text style={styles.text}>$ {box.totalAmount!}</Text>
            <Moment element={Text} format="D MMM YYYY" style={styles.date}>
              {box.closedAt}
            </Moment>
          </View>
        </View>
      </View>
    </TouchableRipple>
  );
};

const styles = StyleSheet.create({
  charge: {
    padding: spacing.normal,
  },
  firstRow: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 18,
  },
  date: {
    fontSize: spacing.medium,
  },
  icon: {
    marginRight: 8,
    backgroundColor: color.dim,
  },
  left: {
    // flexDirection: 'row',
    // flexWrap: 'wrap',
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',
  },
});

export default BoxListItem;
