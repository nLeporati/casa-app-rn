import React, {useState} from 'react';
import {Portal, FAB} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import {color} from '../theme/color';
import {NavigationStackProp} from 'react-navigation-stack';

type BoxFabProps = {
  navigation: NavigationStackProp;
};

const BoxFAB: React.FC<BoxFabProps> = ({navigation}) => {
  const [openFAB, setOpenFAB] = useState(false);

  return (
    <Portal>
      <FAB.Group
        visible
        fabStyle={styles.FAB}
        open={openFAB}
        icon={openFAB ? 'chevron-left' : 'menu'}
        actions={[
          // {
          //   icon: 'contacts',
          //   label: 'People',
          //   onPress: () => console.log('Pressed email'),
          // },
          {
            icon: 'account-supervisor',
            label: 'My group',
            onPress: () => navigation.navigate('Group'),
          },
          {
            icon: 'animation',
            label: 'Boxes List',
            onPress: () => navigation.navigate('BoxesList'),
          },
          // {
          //   icon: 'cash',
          //   label: 'My Charges',
          //   onPress: () => console.log('Pressed email'),
          // },
          {
            icon: 'play',
            label: 'Actived Box',
            onPress: () => navigation.navigate('Box'),
          },
        ]}
        onStateChange={({open}) => setOpenFAB(open)}
        onPress={() => {
          if (openFAB) {
            // do something if the speed dial is open
          }
        }}
      />
    </Portal>
  );
};

const styles = StyleSheet.create({
  FAB: {
    backgroundColor: color.primary,
  },
});

export default BoxFAB;
