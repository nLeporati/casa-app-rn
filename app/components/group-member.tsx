import React, {useState} from 'react';
import {View, StyleSheet, TouchableHighlight, Text} from 'react-native';
import {
  Avatar,
  Title,
  TextInput,
  Button,
  Portal,
  Dialog,
  Paragraph,
  Caption,
} from 'react-native-paper';
import {UserModel} from '../models/user.model';
import {spacing, color} from '../theme';

type GroupMemberProps = {
  user: UserModel;
  onDelete: Function;
  onEditing: Function;
};

const GroupMember: React.FC<GroupMemberProps> = ({
  user,
  onDelete,
  onEditing,
}) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [dialogVisble, setDialogVisible] = useState<boolean>(false);
  const [name, setName] = useState<string>(user.name);

  const toggleEditing = () => {
    setIsEditing(!isEditing);
  };

  const confirm = () => {
    setIsEditing(false);
    user.name = name;
    onEditing(user);
  };

  return (
    <TouchableHighlight
      onPress={() => toggleEditing()}
      onLongPress={() => setDialogVisible(true)}
      underlayColor={color.transparent}>
      <View key={user._id} style={styles.user}>
        <Avatar.Text label={user.avatarText} size={64} />
        {!isEditing && <Title style={styles.userName}>{user.name}</Title>}
        {isEditing && (
          <TextInput
            style={styles.input}
            mode="outlined"
            label="Name"
            value={name}
            onChange={({nativeEvent}) => setName(nativeEvent.text)}
            onSubmitEditing={() => confirm()}></TextInput>
        )}
        {isEditing && <Button icon="cancel">{''}</Button>}
        <Portal>
          <Dialog
            visible={dialogVisble}
            onDismiss={() => setDialogVisible(false)}>
            <Dialog.Title>Remove member</Dialog.Title>
            <Dialog.Content>
              <Paragraph>
                Are you sure to remove {user.name} from the members list?
              </Paragraph>
              <Caption>in the history this member will continue to see</Caption>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => setDialogVisible(false)}>
                <Text style={styles.dialogCancel}>Cancel</Text>
              </Button>
              <Button onPress={() => onDelete(user._id)}>Yes</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: spacing.normal,
    width: 350,
  },
  userName: {
    marginLeft: spacing.normal,
  },
  input: {
    marginLeft: spacing.normal,
    width: 200,
  },
  dialogCancel: {
    color: color.dim,
  },
});

export default GroupMember;
