import React from 'react';
import {Text, StyleProp, TextStyle} from 'react-native';

type CurrencyProps = {
  value: number | string;
  style?: StyleProp<TextStyle>;
};

const Currency: React.FC<CurrencyProps> = ({value, style}) => {
  return <Text style={style}>$ {value || 0}</Text>;
};

export default Currency;
