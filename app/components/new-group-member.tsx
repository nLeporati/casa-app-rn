import React from 'react';
import {View} from 'react-native';
import {TextInput} from 'react-native-paper';

type NewGroupMemberProps = {
  onSuccess: Function;
};

const NewGroupMember: React.FC<NewGroupMemberProps> = ({onSuccess}) => {
  const addNewMember = (name: string) => {
    onSuccess(name);
  };

  return (
    <View>
      <TextInput
        mode="outlined"
        label="New Member"
        onSubmitEditing={({nativeEvent}) =>
          addNewMember(nativeEvent.text)
        }></TextInput>
    </View>
  );
};

export default NewGroupMember;
