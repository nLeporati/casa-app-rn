import {UserModel} from './user.model';
import {BoxModel} from './box.model';

export interface GroupModel {
  _id: string;
  name: string;
  users: UserModel[];
  boxes: BoxModel[]; // TODO ya no va
  activeBox?: string;
  ceratedBy: string;
  createdAt: string;
  updatedAt?: string;
  inviteCode: string;
}
