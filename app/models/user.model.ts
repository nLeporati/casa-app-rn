export interface UserModel {
  _id: string;
  name: string;
  avatarText: string;
  groups: string[];
}
