import {UserModel} from './user.model';
import {ChargeModel} from './charge.model';

export interface CollectionModel {
  collector: UserModel;
  charges: ChargeModel[];
  totalAmount: number;
}

export class Collection implements CollectionModel {
  constructor() {}
  collector!: UserModel;
  charges: ChargeModel[] = [];
  totalAmount!: number;
}
