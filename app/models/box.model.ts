import {UserModel} from './user.model';
import {ChargeModel} from './charge.model';
import {CollectionModel} from './collection-model';
import {CollectModel} from './collect.model';

export interface BoxModel {
  _id: string;
  totalAmount: number;
  createdBy: UserModel;
  name?: string;
  collection?: CollectionModel[]; // TODO ya no va
  collect?: CollectModel[];
  charges: ChargeModel[];
  group: string;
  createdAt: string;
  updatedAt?: string;
  closedAt?: string;
}
