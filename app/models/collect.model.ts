import {UserModel} from './user.model';
import {PaymentModel} from './payment.model';

export interface CollectModel {
  collector: UserModel;
  totalAmount: number;
  payments: PaymentModel[];
}
