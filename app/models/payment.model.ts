import {UserModel} from './user.model';

export interface PaymentModel {
  user: UserModel;
  amount: number;
}
