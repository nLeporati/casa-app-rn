import {UserModel} from './user.model';

export interface ChargeModel {
  _id?: string;
  description: string;
  amount: number;
  payer: UserModel;
  box: string;
  createdAt?: string;
  updatedAt?: string;
}
