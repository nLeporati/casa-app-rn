import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import BoxScreen from '../screens/box/box-screen';
import BoxesListScreen from '../screens/box-list/boxes-list-screen';
import BoxHistoricalScreen from '../screens/box-historical/box-historical.screen';
import GroupScreen from '../screens/group/group.screen';
import HomeScreen from '../screens/home/home.screen';
import AuthLoadingScreen from '../screens/auth-loading/auth-loading.screen';

const AuthStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  },
);

const AppStack = createStackNavigator(
  {
    Box: {screen: BoxScreen},
    Group: {screen: GroupScreen},
    BoxesList: {screen: BoxesListScreen},
    BoxHistorical: {screen: BoxHistoricalScreen},
    // Home: {screen: HomeScreen},
  },
  {
    initialRouteName: 'Box',
    headerMode: 'none',
  },
);

const AppSwitch = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const AppNavigator = createAppContainer(AppSwitch);

export default AppNavigator;
