import React from 'react';
import {GroupStore} from '../stores/group-store';
import {SessionStore} from '../stores/user-store';

export type StoreContext = {
  groupStore: GroupStore;
  sessionStore: SessionStore;
};

const storesContext = React.createContext<StoreContext>({
  groupStore: new GroupStore(),
  sessionStore: new SessionStore(),
});

export default storesContext;
