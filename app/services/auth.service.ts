import axiosClient from '../config/axios.client';
import {AccountModel} from '../models/account.model';
import {JwtToken} from '../models/jwt-token';

class AuthService {
  private static readonly apiUrl = 'auth/';

  static async signup(account: AccountModel): Promise<void | undefined> {
    try {
      await axiosClient.post<AccountModel>(`${this.apiUrl}signup`, account);
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async login(account: AccountModel): Promise<string | undefined> {
    try {
      const {data} = await axiosClient.post<JwtToken>(
        `${this.apiUrl}login`,
        account,
      );
      return data.access_token;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }
}

export default AuthService;
