import axiosClient from '../config/axios.client';
import {UserModel} from '../models/user.model';

class UserService {
  private static readonly apiUrl = 'users/';

  static async createUser(name: string): Promise<UserModel | undefined> {
    try {
      const {data} = await axiosClient.post<UserModel>(`${this.apiUrl}`, {
        name,
      });
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async updateUser(user: UserModel): Promise<UserModel | undefined> {
    try {
      const {data} = await axiosClient.put<UserModel>(
        `${this.apiUrl}${user._id}`,
        user,
      );
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }
}

export default UserService;
