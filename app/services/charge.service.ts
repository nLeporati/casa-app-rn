import axiosClient from '../config/axios.client';
import {GroupModel} from '../models/group.model';
import {ChargeModel} from '../models/charge.model';

class ChargeService {
  private static readonly apiUrl = 'charges/';

  static async createCharge(
    charge: ChargeModel,
  ): Promise<ChargeModel | undefined> {
    try {
      const {data} = await axiosClient.post<ChargeModel>(
        `${this.apiUrl}`,
        charge,
      );
      return data;
    } catch (error) {
      console.log('axios error', error);
    }
  }

  static async deleteCharge(id: string): Promise<void> {
    try {
      await axiosClient.delete(`${this.apiUrl}${id}`);
    } catch (error) {
      console.log('axios error', error);
    }
  }
}

export default ChargeService;
