import axiosClient from '../config/axios.client';
import {BoxModel} from '../models/box.model';
import {CollectModel} from '../models/collect.model';
import {AxiosError} from 'axios';

class BoxService {
  private static readonly apiUrl = 'box/';

  static async getBox(id: string): Promise<BoxModel | undefined> {
    try {
      const {data} = await axiosClient.get<BoxModel>(`${this.apiUrl}${id}`);
      return data;
    } catch (error) {
      console.log('axios error', error);
    }
  }

  static async getHistorical(userId: string): Promise<BoxModel[]> {
    try {
      const {data} = await axiosClient.get<BoxModel[]>(
        `${this.apiUrl}historical/${userId}`,
      );
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async createBox(box: any): Promise<void> {
    try {
      await axiosClient.post(`${this.apiUrl}`, box);
    } catch (error) {
      console.log('axios error', error);
    }
  }

  static async collect(id: string): Promise<CollectModel[] | undefined> {
    try {
      const {data} = await axiosClient.get<CollectModel[]>(
        `${this.apiUrl}${id}/collect`,
      );
      return data;
    } catch (error) {
      console.log('axios error', error);
    }
  }

  static async confirmCollect(id: string): Promise<void> {
    try {
      await axiosClient.get<void>(`${this.apiUrl}${id}/collect/confirm`);
    } catch (error) {
      console.log('axios error', error);
    }
  }
}

export default BoxService;
