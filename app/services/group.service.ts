import axiosClient from '../config/axios.client';
import {GroupModel} from '../models/group.model';

class GroupService {
  private static readonly apiUrl = 'groups/';

  static async getGroup(id: string): Promise<GroupModel | undefined> {
    try {
      const {data} = await axiosClient.get<GroupModel>(`${this.apiUrl}${id}`);
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async createGroup(
    name: string,
    userId: string,
  ): Promise<GroupModel | undefined> {
    try {
      const {data} = await axiosClient.post<GroupModel>(`${this.apiUrl}`, {
        name,
        createdBy: userId,
      });
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async joinGroup(
    user: string,
    inviteCode: string,
  ): Promise<GroupModel | undefined> {
    try {
      const {data} = await axiosClient.post<GroupModel>(`${this.apiUrl}join`, {
        user,
        inviteCode,
      });
      return data;
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async updateActiveGroup(
    id: string,
    boxId: string | undefined,
  ): Promise<GroupModel | undefined> {
    try {
      const {data} = await axiosClient.post<GroupModel>(`${this.apiUrl}${id}`, {
        activeGroup: boxId,
      });
      return data;
    } catch (error) {
      console.log('axios error', error);
    }
  }

  static async addMember(groupId: string, userId: string): Promise<void> {
    try {
      await axiosClient.put(`${this.apiUrl}${groupId}/users/${userId}`);
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }

  static async removeMemeber(groupId: string, userId: string): Promise<void> {
    try {
      await axiosClient.delete(`${this.apiUrl}${groupId}/users/${userId}`);
    } catch (error) {
      console.log('axios error', error);
      throw error;
    }
  }
}

export default GroupService;
