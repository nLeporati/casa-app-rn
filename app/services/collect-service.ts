import DATA_BOXES from '../data/data-boxes';
import {CollectionModel} from '../models/collection-model';
import DATA_GROUPS from '../data/data-groups';

class CollectService {
  public getCollects = (boxId: string) => {
    const group = DATA_GROUPS[0];
    const box = DATA_BOXES.find(box => box._id);

    group.users.forEach(user => {
      const userPays = box?.charges
        .filter(charge => charge.payer._id === user._id)
        .map(charge => charge.amount)
        .reduce((total, amount) => total + amount);
    });
  };
}

export default CollectService;
