import React from 'react';
import {Provider as PaperProvider, DefaultTheme} from 'react-native-paper';
import {StatusBar} from 'react-native';
import {color} from './app/theme/color';
import AppNavigator from './app/navigation/stack-navigator';
import storesContext, {StoreContext} from './app/contexts/storesContext';
import {SessionStore} from './app/stores/user-store';
import {GroupStore} from './app/stores/group-store';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: color.primary,
    // accent: '#f1c40f',
  },
};

const appContext: StoreContext = {
  groupStore: new GroupStore(),
  sessionStore: new SessionStore(),
};

const App = () => {
  return (
    <>
      <PaperProvider theme={theme}>
        <StatusBar barStyle="dark-content" />
        <storesContext.Provider value={appContext}>
          <AppNavigator />
        </storesContext.Provider>
      </PaperProvider>
    </>
  );
};

export default App;
